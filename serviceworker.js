const staticCacheName = 'pwa-v1';

const addToCache = async (request, response) => {
  const cache = await caches.open(staticCacheName);
  cache.put(request, response.clone());
};

self.addEventListener('install', (e) => {
  e.waitUntil(caches.open(staticCacheName).then(cache => cache.addAll(['/'])));
});

self.addEventListener('activate', (e) => {
  e.waitUntil(
    caches.keys().then(keys => Promise.all(
      keys.map(key => key !== staticCacheName && caches.delete(key))
    ))
  );
});

self.addEventListener('fetch', (e) => {
  if (e.request.method !== 'GET') return e.respondWith(fetch(e.request));
  e.respondWith(
    caches.match(e.request).then(cached => cached || fetch(e.request).then(async (response) => {
      await addToCache(e.request, response);
      return response;
    }))
  );
});
