// Initialize deferredPrompt for use later to show browser install prompt.
let deferredPrompt;

window.addEventListener('beforeinstallprompt', (e) => {
  // Prevent the mini-infobar from appearing on mobile
  e.preventDefault();
  // Stash the event so it can be triggered later.
  deferredPrompt = e;
  // Optionally, send analytics event that PWA install promo was shown.
  console.log(`'beforeinstallprompt' event was fired.`);
});

window.addEventListener('load', () => {
    registerServiceWorker();
});

// Register the Service Worker
async function registerServiceWorker() {
    if ('serviceWorker' in navigator) {
        try {
        await navigator
                .serviceWorker
                .register('serviceworker.js');
        }
        catch (e) {
            console.log('ServiceWorker registration failed');
        }
    }
}