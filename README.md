# README.md för Webmanifest och Progressive Web Apps (PWA)

## Inledning

Den här README.md-filen syftar till att ge en grundläggande förståelse för hur webmanifest och Progressive Web Apps (PWA) fungerar. Filerna i detta projekt gör det möjligt att installera din webbapplikation som en app på både datorer och telefoner.

## Filstruktur

Projektet innehåller följande filer:

```
├── appify.js          # Hanterar PWA-logik
├── logo.png           # App-ikon
├── manifest.json      # Webmanifest för PWA
├── README.md          # Dokumentation
└── serviceworker.js   # Service Worker för offline-stöd
```

## Steg för att komma igång

För att komma igång bör du lägga samtliga filer i detta repository i root-mappen på din webbsida. Du bör sedan uppdatera informationen i `manifest.json` till din specifika information och (om du vill) byta ut ikonen till en som passar din app bättre.

### 1. Lägg till `manifest.json` i ditt projekt

`manifest.json` är en konfigurationsfil som definierar hur din app ska bete sig när den är installerad. Den innehåller metadata som appens namn, ikoner och start-URL.

Exempel på `manifest.json`:

```json
{
    "name": "QuickbotAI",
    "short_name": "Quickbot",
    "theme_color": "#2980B9",
    "background_color": "#2980B9",
    "start_url": "https://student.oedu.se/~mh6802/quickbot",
    "display": "fullscreen",
    "orientation": "portrait",
    "description": "A virtual assistant",
    "icons": [{
        "src": "logo.png",
        "type": "image/png",
        "sizes": "256x256"
    }],
    "scope": "https://student.oedu.se/~mh6802/quickbot",
    "id": "https://student.oedu.se/~mh6802/quickbot"
}
```

Lägg till en länk till `manifest.json` i `<head>`-sektionen av din HTML-fil:

```html
<link rel="manifest" href="manifest.json">
```

### 2. Lägg till en Service Worker via `serviceworker.js`

Service Workers agerar som en proxy mellan webbläsaren och nätverket och kan hantera saker som cachning och offline-stöd.

Exempel på `serviceworker.js`:

```javascript
const staticCacheName = 'pwa-v1';

const addToCache = async (request, response) => {
  const cache = await caches.open(staticCacheName);
  cache.put(request, response.clone());
};

self.addEventListener('install', (e) => {
  e.waitUntil(caches.open(staticCacheName).then(cache => cache.addAll(['/'])));
});

self.addEventListener('activate', (e) => {
  e.waitUntil(
    caches.keys().then(keys => Promise.all(
      keys.map(key => key !== staticCacheName && caches.delete(key))
    ))
  );
});

self.addEventListener('fetch', (e) => {
  if (e.request.method !== 'GET') return e.respondWith(fetch(e.request));
  e.respondWith(
    caches.match(e.request).then(cached => cached || fetch(e.request).then(async (response) => {
      await addToCache(e.request, response);
      return response;
    }))
  );
});
```

### 3. Registrera Service Worker i `appify.js`

Använd `appify.js` för att registrera din Service Worker.

Exempel på `appify.js`:

```javascript
// Initialize deferredPrompt for use later to show browser install prompt.
let deferredPrompt;

window.addEventListener('beforeinstallprompt', (e) => {
  // Prevent the mini-infobar from appearing on mobile
  e.preventDefault();
  // Stash the event so it can be triggered later.
  deferredPrompt = e;
  // Optionally, send analytics event that PWA install promo was shown.
  console.log(`'beforeinstallprompt' event was fired.`);
});

window.addEventListener('load', () => {
    registerServiceWorker();
});

// Register the Service Worker
async function registerServiceWorker() {
    if ('serviceWorker' in navigator) {
        try {
        await navigator
                .serviceWorker
                .register('serviceworker.js');
        }
        catch (e) {
            console.log('ServiceWorker registration failed');
        }
    }
}
```

### 4. Lägg till App-ikon

Lägg `logo.png` i din projektmapp och referera till den i `manifest.json`.

## Testa din PWA

För att testa att allt fungerar, öppna din webbapplikation i en webbläsare som stöder PWAs (t.ex. Chrome eller Firefox). Du bör se en möjlighet att installera webbapplikationen som en app.

## Slutsats

Genom att följa dessa steg borde du nu ha en grundläggande Progressive Web App (PWA) som kan installeras som en app på datorer och telefoner. För mer avancerade funktioner och anpassningar, referera till den officiella dokumentationen för Webmanifest och Service Workers.